package infra

import (
	"database/sql"
	"fmt"
	"log"
	"sync"
	"time"

	"github.com/spf13/viper"
	"github.com/go-sql-driver/mysql"
	sqltrace "gopkg.in/DataDog/dd-trace-go.v1/contrib/database/sql"

	"template-b/model"
)

type Infra interface {
	Config() *viper.Viper
	DB() *DB 
}

type infraCtx struct{
	configFilePath string
}

type DB struct {
	Master, Slave *sql.DB
}

func New() Infra {
	return &infraCtx{
		configFilePath: "config/app.toml",
	}
}

var (
	cfgOnce              sync.Once
	cfg                  *viper.Viper
)

func (c *infraCtx) Config() *viper.Viper {
	cfgOnce.Do(func() {
		viper.SetConfigFile(c.configFilePath)
		if err := viper.ReadInConfig(); err != nil {
			log.Fatal(err)
		}

		cfg = viper.GetViper()
	})

	return cfg
}

var (
	sqlDBOnce sync.Once
	sqlDB     DB
)

func connectMySQL(dbConf model.DBConfig, ddConfigService string) *sql.DB {
	connString := fmt.Sprintf(
		"%s:%s@tcp(%s:%d)/%s",
		dbConf.User,
		dbConf.Password,
		dbConf.Host,
		dbConf.Port,
		dbConf.Database,
	)

	sqltrace.Register("mysql", &mysql.MySQLDriver{}, sqltrace.WithServiceName(ddConfigService))
	db, err := sqltrace.Open("mysql", connString)
	if err != nil {
		log.Fatal(err)
	}

	db.SetConnMaxLifetime(time.Duration(dbConf.MaxLifeTime) * time.Second)
	db.SetMaxIdleConns(dbConf.MaxIdleConns)
	db.SetMaxOpenConns(dbConf.MaxOpenConn)

	return db
}

func (c *infraCtx) DB() *DB {
	sqlDBOnce.Do(func() {
		dbConfig := c.Config().Sub("db_master")
		dbConfMaster := model.DBConfig{
			Host:         dbConfig.GetString("host"),
			Port:         dbConfig.GetInt("port"),
			Database:     dbConfig.GetString("database"),
			User:         dbConfig.GetString("user"),
			Password:     dbConfig.GetString("password"),
			MaxLifeTime:  dbConfig.GetInt("max_lifetime"),
			MaxIdleConns: dbConfig.GetInt("max_idle_con"),
			MaxOpenConn:  dbConfig.GetInt("max_open_con"),
		}

		ddConfig := c.Config().Sub("datadog")
		sqlDB.Master = connectMySQL(dbConfMaster, ddConfig.GetString("service"))

		// Slave Conection
		dbConfig = c.Config().Sub("db_slave")
		dbConfSlave := model.DBConfig{
			Host:         dbConfig.GetString("host"),
			Port:         dbConfig.GetInt("port"),
			Database:     dbConfig.GetString("database"),
			User:         dbConfig.GetString("user"),
			Password:     dbConfig.GetString("password"),
			MaxLifeTime:  dbConfig.GetInt("max_lifetime"),
			MaxIdleConns: dbConfig.GetInt("max_idle_con"),
			MaxOpenConn:  dbConfig.GetInt("max_open_con"),
		}

		sqlDB.Slave = connectMySQL(dbConfSlave, ddConfig.GetString("service"))
	})

	return &sqlDB
}