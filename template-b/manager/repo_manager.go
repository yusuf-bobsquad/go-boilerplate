package manager

import (
	"sync"

	"template-b/infra"
	"template-b/interfaces"
	"template-b/repo"
)

type RepoManager interface {
	ExampleRepo() interfaces.IExampleRepo
}

type repoManager struct {
	infra  infra.Infra
}

func NewRepoManager(infra infra.Infra) RepoManager {
	return &repoManager{
		infra: infra,
	}
}

var (
	exampleRepoOnce sync.Once
	exampleRepo     interfaces.IExampleRepo
)

func (c *repoManager) ExampleRepo() interfaces.IExampleRepo {
	exampleRepoOnce.Do(func(){
		exampleRepo = repo.NewExampleRepo(c.infra.DB())
	})

	return exampleRepo
}