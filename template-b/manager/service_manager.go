package manager

import (
	"sync"

	"template-b/infra"
	"template-b/interfaces"
	"template-b/service"
)

type ServiceManager interface {
	ExampleService() interfaces.IExampleService
}

type serviceManager struct {
	infra  infra.Infra
	repo   RepoManager
}

func NewServiceManager(data infra.Infra) ServiceManager {
	return &serviceManager{
		infra: data,
		repo: NewRepoManager(data),
	}
}

var (
	exampleServiceOnce sync.Once
	exampleService     interfaces.IExampleService
)

func (c *serviceManager) ExampleService() interfaces.IExampleService {
	exampleServiceOnce.Do(func(){
		exampleService = service.NewExampleService(c.repo.ExampleRepo())
	})

	return exampleService
}