package interfaces

import (
	"context"
)

type IExampleService interface {
	ExCallService(ctx context.Context) error
}
