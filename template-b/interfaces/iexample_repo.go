package interfaces

import (
	"context"
)

type IExampleRepo interface {
	ExRepoCall(ctx context.Context) error
}