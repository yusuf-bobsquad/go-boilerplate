package v1

import (
	"net/http"

	"template-b/interfaces"
	"template-b/shared/http/response"
)

type exampleHandlerCtx struct {
	jsonResponder 	response.JSONResponder
	exampleService 	interfaces.IExampleService
}

func NewExampleHandler(exampleService interfaces.IExampleService, jsonResponder response.JSONResponder) *exampleHandlerCtx {
	return &exampleHandlerCtx{
		exampleService: exampleService,
		jsonResponder: jsonResponder,
	}
}

func (c *exampleHandlerCtx) ExampleGet(w http.ResponseWriter, r *http.Request) {
	err := c.exampleService.ExCallService(r.Context())

	if err != nil {
		c.jsonResponder.Write(w, http.StatusInternalServerError, map[string]interface{}{"error": err})
		return
	}

	c.jsonResponder.Write(w, http.StatusOK, map[string]interface{}{"success": true})
}