package v1

import (
	"reflect"
	"testing"
	"net/http"
	"net/http/httptest"
	"errors"

	"github.com/golang/mock/gomock"

	"template-b/shared/http/response"
	"template-b/interfaces"
	mock_interfaces "template-b/mocks"
)

// TestNewExampleHandler ...
func TestNewExampleHandler(t *testing.T) {
	type args struct {
		jsonResponder response.JSONResponder
		exampleService interfaces.IExampleService
	}
	tests := []struct {
		name string
		args args
		want func() *exampleHandlerCtx
	}{
		{
			name: "success",
			args: args{
				jsonResponder: response.NewDefaultJSONResponder(),
				exampleService: &mock_interfaces.MockIExampleService{},
			},
			want: func() *exampleHandlerCtx {
				return &exampleHandlerCtx{
					jsonResponder: response.NewDefaultJSONResponder(),
					exampleService: &mock_interfaces.MockIExampleService{},
				}
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewExampleHandler(tt.args.exampleService, tt.args.jsonResponder)
			want := tt.want()
			if !reflect.DeepEqual(got, want) {
				t.Errorf("NewExampleHandler() = %v, want %v", got, want)
			}
		})
	}
}

func Test_exampleHandlerCtxr_ExampleGet(t *testing.T) {
	type fields struct {
		jsonResponder  response.JSONResponder
		exampleService interfaces.IExampleService
	}
	tests := []struct {
		name          string
		fields        fields
		initRouter    func() (*http.Request, *httptest.ResponseRecorder)
		doMockService func(mock *mock_interfaces.MockIExampleService)
	}{
		{
			name: "unexpected error on exampleService.ExCallService",
			fields: fields{
				jsonResponder: response.NewDefaultJSONResponder(),
				exampleService: &mock_interfaces.MockIExampleService{},
			},
			initRouter: func() (*http.Request, *httptest.ResponseRecorder) {
				r, _ := http.NewRequest(http.MethodGet, "/example", nil)
				w := httptest.NewRecorder()

				return r, w
			},
			doMockService: func(mock *mock_interfaces.MockIExampleService) {
				mock.EXPECT().ExCallService(gomock.Any()).Return(errors.New("unexpected error"))
			},
		},
		{
			name: "success",
			fields: fields{
				jsonResponder: response.NewDefaultJSONResponder(),
				exampleService: &mock_interfaces.MockIExampleService{},
			},
			initRouter: func() (*http.Request, *httptest.ResponseRecorder) {
				r, _ := http.NewRequest(http.MethodGet, "/example", nil)
				w := httptest.NewRecorder()

				return r, w
			},
			doMockService: func(mock *mock_interfaces.MockIExampleService) {
				mock.EXPECT().ExCallService(gomock.Any()).Return(nil)
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &exampleHandlerCtx{
				jsonResponder:  tt.fields.jsonResponder,
				exampleService: tt.fields.exampleService,
			}

			mockCtrl := gomock.NewController(t)
			defer mockCtrl.Finish()

			mockService := mock_interfaces.NewMockIExampleService(mockCtrl)
			c.exampleService = mockService
			tt.doMockService(mockService)

			r, w := tt.initRouter()
			c.ExampleGet(w, r)
		})
	}
}