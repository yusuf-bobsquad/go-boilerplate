package service

import (
	"context"

	"template-b/interfaces"
)

type exampleServiceCtx struct {
	exRepo interfaces.IExampleRepo
}

func NewExampleService(exRepo interfaces.IExampleRepo) interfaces.IExampleService {
	return &exampleServiceCtx{
		exRepo: exRepo,
	}
}

func (c *exampleServiceCtx) ExCallService(ctx context.Context) error {
	err := c.exRepo.ExRepoCall(ctx)
	return err
}