package service

import (
	"reflect"
	"testing"
	"context"
	"errors"

	"github.com/golang/mock/gomock"

	"template-b/interfaces"
	mock_interfaces "template-b/mocks"
)

func TestNewExampleService(t *testing.T) {
	type args struct {
		exRepo interfaces.IExampleRepo
	}

	tests := []struct {
		name string
		args args
		want func() *exampleServiceCtx
	}{
		{
			name: "success",
			args: args{
				exRepo: &mock_interfaces.MockIExampleRepo{},
			},
			want: func() *exampleServiceCtx {
				return &exampleServiceCtx{
					exRepo: &mock_interfaces.MockIExampleRepo{},
				}
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewExampleService(tt.args.exRepo)
			want := tt.want()
			if !reflect.DeepEqual(got, want) {
				t.Errorf("NewExampleService() = %v, want %v", got, want)
			}
		})
	}
}

func Test_exampleServiceCtx_ExCallService(t *testing.T) {
	type fields struct {
		exRepo interfaces.IExampleRepo
	}

	type args struct {
		ctx context.Context
	}

	tests := []struct {
		name          string
		fields        fields
		args args
		doMock func(mock *mock_interfaces.MockIExampleRepo)
		wantErr error
	}{
		{
			name: "unexpected error on exRepo.ExRepoCall",
			args: args{
				ctx: context.Background(),
			},
			doMock: func(mock *mock_interfaces.MockIExampleRepo){
				mock.EXPECT().ExRepoCall(gomock.Any()).Return(errors.New("unexpected error"))
			},
			wantErr: errors.New("unexpected error"),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &exampleServiceCtx{
				exRepo:   tt.fields.exRepo,
			}

			mockCtrl := gomock.NewController(t)
			defer mockCtrl.Finish()

			mockRepo := mock_interfaces.NewMockIExampleRepo(mockCtrl)
			tt.doMock(mockRepo)
			c.exRepo = mockRepo

			err := c.ExCallService(tt.args.ctx)
			if err != nil {
				if err.Error() != tt.wantErr.Error() {
					t.Errorf("exampleServiceCtx.ExCallService() error = %v, wantErr %v", err, tt.wantErr)
					return
				}
			}
		})
	}
}