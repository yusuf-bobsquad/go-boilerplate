package api

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

const (
	healthHeaderToken = "x-health-token"
)

var (
	isShuttingDown bool

	healthyMessage = func() map[string]string {
		return map[string]string{
			"en": "service is alive",
			"id": "service sedang berjalan",
		}
	}
	unHealthyMessage = func() map[string]string {
		return map[string]string{
			"en": "service is not healthy",
			"id": "service sedang tidak sehat",
		}
	}
	shuttingDownMessage = func() map[string]string {
		return map[string]string{
			"en": "service is shutting down",
			"id": "service akan mati",
		}
	}

	defaultHealthyResponse = func() healthResponse {
		return healthResponse{
			Message: healthyMessage(),
		}
	}
)

// Persistence represents a generic struct model for persistence
// Name is free text. i.e: shipment
// type is free text. i.e: postgreSQL, redis, etc
type Persistence struct {
	Name      string  `json:"name"`
	Type      string  `json:"type"`
	Status    string  `json:"status"`
	PingError *string `json:"ping_error,omitempty"`

	HealthPersistence HealthPersistence `json:"-"`
}

// NewPersistence to create new Instance for Persistence struct
func NewPersistence(name, persistenceType string, hp HealthPersistence) Persistence {
	return Persistence{
		Name:              name,
		Type:              persistenceType,
		HealthPersistence: hp,
	}
}

// Persistences represents array of Persistence
type Persistences []Persistence

// Ping will check each persintece status
// and assign status field for Persistence
// one persistence failed, will return false
// it indicates that service is unhealthy
func (p *Persistences) Ping() (ok bool) {
	ok = true
	for i, persistance := range *p {
		if err := persistance.HealthPersistence.Ping(); err != nil {
			errMsg := err.Error()
			persistance.Status = "FAILED"
			persistance.PingError = &errMsg
			ok = false
		} else {
			persistance.Status = "OK"
			persistance.PingError = nil
		}
		(*p)[i] = persistance
	}
	return
}

// HealthPersistence is interface contains methods those need to be implemented by the actual persistence
type HealthPersistence interface {
	Ping() error
}

type healthResponse struct {
	ErrorID      *int              `json:"errorid,omitempty"`
	Message      map[string]string `json:"message"`
	Persistences *Persistences     `json:"persistences,omitempty"`
}

func (resp healthResponse) ToJSONMarshal() []byte {
	js, _ := json.Marshal(resp)
	return js
}

// HealthHandler contains list of persistences those need to be checked
// and also the delay duration before the service will be killed
type HealthHandler struct {
	persistences          *Persistences
	shutdownDelayDuration time.Duration

	token *string
}

func NewHealthHandler(persistences *Persistences, shutdownDelayDuration time.Duration) (*HealthHandler, error) {
	if persistences == nil {
		return nil, fmt.Errorf("persistences can't be nil")
	}

	handler := &HealthHandler{
		persistences:          persistences,
		shutdownDelayDuration: shutdownDelayDuration,
	}
	handler.gracefulShutdown()
	return handler, nil
}

func (h *HealthHandler) WithToken(token string) {
	h.token = &token
}

func (h HealthHandler) IsShuttingDown() (bool, *healthResponse) {
	if isShuttingDown {
		resp := healthResponse{}
		shuttingDownErrorID := http.StatusServiceUnavailable
		resp.ErrorID = &shuttingDownErrorID
		resp.Message = shuttingDownMessage()
		return true, &resp
	}
	return false, nil
}

func (h HealthHandler) gracefulShutdown() {
	stopChan := make(chan os.Signal)
	signal.Notify(stopChan, os.Interrupt, syscall.SIGTERM)
	go h.listenToSigTerm(stopChan)
}

func (h HealthHandler) listenToSigTerm(stopChan chan os.Signal) {
	<-stopChan
	log.Println("Shutting down service... Will be killed on", h.shutdownDelayDuration)
	isShuttingDown = true

	time.Sleep(h.shutdownDelayDuration)
	log.Println("Bye..")
	os.Exit(0)
}

// GetHealth is an handler for health endpoint
func (h HealthHandler) GetHealth(w http.ResponseWriter, r *http.Request) {
	resp := defaultHealthyResponse()
	w.Header().Set("Content-Type", "application/json")

	// if use WithToken, the handler will validate token from incoming request
	// and if it doesn't match, will throw unauthorized error
	if h.token != nil && r.Header.Get(healthHeaderToken) != *h.token {
		errMsg := healthResponse{
			Message: map[string]string{
				"en": "Unauthorized",
			},
		}
		w.WriteHeader(http.StatusUnauthorized)
		w.Write(errMsg.ToJSONMarshal())
		return
	}

	if isShuttingDown, tempResp := h.IsShuttingDown(); isShuttingDown {
		w.WriteHeader(http.StatusServiceUnavailable)
		w.Write(tempResp.ToJSONMarshal())
		return
	}

	resp.Persistences = h.persistences
	if resp.Persistences.Ping() {
		w.WriteHeader(http.StatusOK)
	} else {
		pingFailedErrorID := http.StatusServiceUnavailable
		resp.ErrorID = &pingFailedErrorID
		resp.Message = unHealthyMessage()
		w.WriteHeader(http.StatusServiceUnavailable)
	}
	w.Write(resp.ToJSONMarshal())
}
