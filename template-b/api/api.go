package api

import (
	"log"
	"time"
	"net/http"
	"fmt"

	"github.com/go-chi/chi"

	"template-b/infra"
	"template-b/manager"
	"template-b/shared/http/response"
	v1 "template-b/handler/v1"
)

type Server interface {
	Run()
}

type server struct {
	router         chi.Router
	infra          infra.Infra
	serviceManager manager.ServiceManager
	jsonResponder response.JSONResponder
}

// NewServer construct new API server
func NewServer(infra infra.Infra) Server {

	return &server{
		router:         chi.NewRouter(),
		infra:          infra,
		serviceManager: manager.NewServiceManager(infra),
		jsonResponder:  response.NewDefaultJSONResponder(),
	}
}

// Run serve HTTP
func (c *server) Run() {
	c.handlers()
	c.run()
}

func (c *server) prepareHealth() *HealthHandler {
	// load health config from envar
	healthCfg := c.infra.Config().Sub("health")
	if healthCfg == nil {
		log.Fatal("failed to load health config")
	}
	gracefulTimeout := time.Duration(healthCfg.GetInt("graceful_timeout")) * time.Second

	// prepare DB
	sqlDB := c.infra.DB()
	sqlDBMaster := NewPersistence("master", "MySQL", sqlDB.Master)
	sqlDBSlave := NewPersistence("slave", "MySQL", sqlDB.Slave)

	persistences := Persistences{sqlDBMaster, sqlDBSlave}

	healthHandler, err := NewHealthHandler(&persistences, gracefulTimeout)
	if err != nil {
		log.Fatal("failed to init healthHandler")
	}

	return healthHandler
}

func (c *server) handlers() {
	// set healthcheck
	healthHandler := c.prepareHealth()
	c.router.Get("/health", healthHandler.GetHealth)

	// set more configuration here
	// CORS, etc
	c.endpoint()
}

func (c *server) run() {
	apiConfig := c.infra.Config().Sub("api")
	addr := fmt.Sprintf("%s:%d", apiConfig.GetString("host"), apiConfig.GetInt("port"))
	server := &http.Server{
		Addr:         addr,
		Handler:      c.router,
		ReadTimeout:  time.Duration(apiConfig.GetInt("read_timeout")) * time.Second,
		WriteTimeout: time.Duration(apiConfig.GetInt("write_timeout")) * time.Second,
		IdleTimeout:  time.Duration(apiConfig.GetInt("idle_timeout")) * time.Second,
	}

	if apiConfig.GetBool("debug") {
		log.Println("pprof debug mode")
	}

	if err := server.ListenAndServe(); err != nil {
		log.Fatal("Server start error")
	}
}

func (c *server) endpoint() {
	exampleHandler := v1.NewExampleHandler(c.serviceManager.ExampleService(), c.jsonResponder)

	c.router.Get("/example", exampleHandler.ExampleGet)
}