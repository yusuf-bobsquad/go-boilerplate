package repo

import (
	"context"

	"template-b/infra"
	"template-b/interfaces"
)

type exampleRepoCtx struct {
	db *infra.DB
}

func NewExampleRepo(db *infra.DB) interfaces.IExampleRepo {
	return &exampleRepoCtx{
		db: db,
	}
}

func (c *exampleRepoCtx) ExRepoCall(ctx context.Context) error {
	return nil
}