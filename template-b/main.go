package main

import (
	"os"

	"github.com/urfave/cli"

	"template-b/api"
	"template-b/infra"
)

const (
	// AppName Application name
	AppName = "Go-Service"
	// AppTagLine Application tagline
	AppTagLine = "Boilerplate Go Service"
)

func main() {

	app := cli.NewApp()
	app.Name = AppName
	app.Usage = AppTagLine

	app.Commands = []cli.Command{
		API,
	}

	app.Flags = append(app.Flags)
	_ = app.Run(os.Args)

}

// API run API server
var API = cli.Command{
	Name:     "api",
	Usage:    "Run API Server",
	Action: func(ctx *cli.Context) {
		api.NewServer(infra.New()).Run()
	},
}
