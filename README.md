# Go Boilerplate
[![forthebadge](https://forthebadge.com/images/badges/made-with-go.svg)](https://forthebadge.com) [![forthebadge](https://forthebadge.com/images/badges/fuck-it-ship-it.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/makes-people-smile.svg)](https://forthebadge.com)
 
 ## Requirements
 
  - Golang 
  - Go Module
  - Gomock https://github.com/golang/mock
  - other needed modules
  
 ## Installing
  
   - Use Go Modules, please read https://blog.golang.org/using-go-modules
   - Install Dependecies
      ```console
      $ go mod tidy
      $ go mod vendor
      ```
   - Check coverage unit test
      ```
      $ make coverage
      ```

## Copyright Notice

Copyright &copy; 2022 PT BOBOBOX Mitra Indonesia.

NOTICE:

All Information contained herein is, and remains the property of
PT BOBOBOX Mitra Indonesia. The intellectual
and technical concepts contained herein are proprietary to PT BOBOBOX Mitra Indonesia and maybe covered by Republic of
Indonesia and Foreign Patents, patents in process, and are protected by
trade secret or copyright law. Dissemination of this information or
reproduction of this material is strictly forbidden unless prior written
permission is obtained from PT BOBOBOX Mitra Indonesia.
