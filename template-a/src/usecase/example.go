package usecase

import (
	"context"

	"template-a/interfaces"
)

type ucExample struct {
	repoExample interfaces.IExampleRepository
}

func NewUcExample(repoExample interfaces.IExampleRepository) *ucExample {
	return &ucExample{
		repoExample: repoExample,
	}
}

func (uc *ucExample) ExCallUsecase(ctx context.Context) error {
	err := uc.repoExample.ExCallRepository(ctx)
	return err
}