package src

import (
	"net/http"
	
	"template-a/config"
)

// Server ...
type Server struct {
	config     *config.Config
	httpServer *http.Server
}

// InitServer ...
func InitServer(cfg *config.Config) *Server {
	
}

// Run ...
func (s *Server) Run() {
	// setup http framework here
}