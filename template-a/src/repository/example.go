package repository

import (
	"context"

	"template-a/interfaces"
)

type repoExample struct {
}

func NewRepoExample() *repoExample {
	return &repoExample{
	}
}

func (r *repoExample) ExCallUsecase(ctx context.Context) error {
	return nil
}