package interfaces

import (
	"context"
)

type IExampleRepository interface {
	ExCallRepository(ctx context.Context) error
}