package interfaces

import (
	"context"
)

type IExampleUsecase interface {
	ExCallUsecase(ctx context.Context) error
}