package main

import (
	"flag"
	"fmt"

	dotenv "github.com/joho/godotenv"

	"template-a/config"
	"template-a/src"
)

func main() {
	cmd := flag.String("app", "", "")
	flag.Parse()

	err := dotenv.Load(".env")
	if err != nil {
		panic(".env is not loaded properly")
	}

	cfg := config.NewConfig()
	server := src.InitServer(cfg)

	switch *cmd {
	default:
		server.Run()
	}
}
